import express, { urlencoded } from 'express';
import { json } from 'body-parser';
import db from './db/db';
import routes from './routes/index.routes';

class Server {
  private app: express.Application;
  constructor() {
    this.app = express();
    this.configurationServer();
    this.routes();
    this.listenDatabase();
    this.listenServer();
  }
  private configurationServer() {
    this.app.use(json({ strict: true }));
    this.app.use(urlencoded({ limit: '16mb' }));
  }
  private routes() {
    this.app.use('/api', routes);
  }
  private listenDatabase() {
    db.authentication();
  }
  private listenServer() {
    this.app.listen(3000, () => {
      console.log('listen server on port 3000');
    });
  }
}
export default new Server();