import { DataTypes, Model, CreateOptions, literal, ModelAttributes } from 'sequelize';
import sequalize from '../db/db';
import { OrganizationModel } from './organization.model';

const db = sequalize.getInstanceDb();

export interface IContract{
    contract_Id?: Number;
    contract_Document:String;
    contract_Date:String;
    contract_PaymentDate:String;
    contract_Status:number;
    contract_OrganizationID:Number;
}

export class ContractModel extends Model{
    static init(){
        super.init({
            contract_Id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            contract_Document: {
                type: DataTypes.TEXT({length: 'long'}),
                allowNull: false
            },
            contract_Date: {
                type: 'TIMESTAMP',
                defaultValue: literal('CURRENT_TIMESTAMP'),
                allowNull: true
            },
            contract_PaymentDate: {
                type: DataTypes.DATE,
                allowNull: false
            },
            contract_Status: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            contract_OrganizationID: {
                type: DataTypes.INTEGER,
                references: {
                    model: OrganizationModel,
                    key: 'organization_Id'
                },
                allowNull: false
            }
        },
        {
            tableName: 'Contract',
            freezeTableName: true,
            sequelize: db,
            timestamps: false
        });
    }
}

ContractModel.init();

ContractModel.belongsTo(OrganizationModel, {
    foreignKey: 'contract_OrganizationID',
    targetKey: 'organization_Id'
});