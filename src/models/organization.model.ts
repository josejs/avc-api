import { DataTypes, Model, CreateOptions } from 'sequelize';
import { hashSync, genSaltSync } from 'bcrypt';
import sequelize from '../db/db';
import { RoleModel } from './roles.model';


const db = sequelize.getInstanceDb();

export interface IOrganization {
  organization_Id?: number;
  organization_Name: string;
  organization_NIT: number;
  organization_Direction: string;
  organization_Email: string;
  organization_Password: string;
  organization_Status: number;
  organization_RoleID: number;
}

export class OrganizationModel extends Model<IOrganization, IOrganization> {
  public organization_Id!: number;
  public organization_Name!: string;
  public organization_NIT!: number;
  public organization_Direction!: string;
  public organization_Email!: string;
  public organization_Password!: string;
  public organization_Status!: number;
  public organization_RoleID!: number;
  static init() {
    super.init({
      organization_Id: {
        type: DataTypes.INTEGER({ length: 11 }),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      organization_Name: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      organization_NIT: {
        type: DataTypes.INTEGER({ length: 11 }),
        unique: true,
        allowNull: false,
      },
      organization_Direction: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      organization_Email: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      organization_Password: {
        type: DataTypes.TEXT({ length: 'long' }),
        allowNull: false
      },
      organization_Status: {
        type: DataTypes.TINYINT({ length: 1 }),
        allowNull: false
      },
      organization_RoleID: {
        type: DataTypes.INTEGER({ length: 11 }),
        references: {
          model: RoleModel,
          key: 'role_Id'
        }
      }
    }, {
        tableName: 'Organization',
        freezeTableName: true,
        timestamps: false,
        sequelize: db,
        hooks: {
          beforeCreate: (organization: any, options: CreateOptions) => {
            const passwordHashed = hashSync(organization.organization_Password, genSaltSync(5));
            organization.organization_Password = passwordHashed;
          },
        },
      });
  }
}
OrganizationModel.init();
OrganizationModel.belongsTo(RoleModel, { 
  foreignKey: 'organization_RoleId', 
  targetKey: 'role_Id' 
});