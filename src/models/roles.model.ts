import { DataTypes, Model } from 'sequelize';
import db from '../db/db';

const sequelize = db.getInstanceDb();

export interface IRole {
  role_id?: number;
  role_Name: string;
  role_Status: number;
}

export class RoleModel extends Model<IRole, IRole> {
  public role_Id!: number;
  public role_Name!: string;
  public role_Status!: number;

  static init() {
    super.init({
      role_Id: {
        type: DataTypes.INTEGER({ length: 11 }),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      role_Name: {
        type: DataTypes.STRING({ length: 45 }),
        allowNull: false,
      },
      role_Status: {
        type: DataTypes.TINYINT({ length: 1 })
      }
    }, { tableName: 'Roles', timestamps: false, freezeTableName: true, sequelize });
  }
}
RoleModel.init();