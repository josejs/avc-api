import { Sequelize } from 'sequelize';

class Db {
  private db: any;
  constructor() {
    this.db = new Sequelize({
      port: 3306,
      dialect: 'mysql',
      username: 'admin',
      password: 'Hoobastank1997?',
      database: 'AVC_DB'
    });
  }
  public authentication(): void {
    this.db.authenticate()
      .then(() => console.log('connected db successfull'))
      .catch((e: any) => console.log('error: ', e));
  }
  public getInstanceDb(): Sequelize {
    return this.db;
  }
}
export default new Db();