import { Router } from 'express';
import OrganizationRouter from './organization.routes';
import RoleRouter from './roles.routes';
import ContractRouter from './contract.routes';


class IndexRouter {
  private router: Router;
  constructor() {
    this.router = Router();
  }
  public builderRoutes(): Router {
    this.router.use('/organization', OrganizationRouter);
    this.router.use('/roles', RoleRouter);
    this.router.use('/contract', ContractRouter);
    return this.router;
  }
}
export default new IndexRouter().builderRoutes();