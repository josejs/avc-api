import { Router, Request, Response } from 'express';
import { IOrganization, OrganizationModel } from '../models/organization.model';
import { RoleModel } from '../models/roles.model';

class OrganizationRouter {
  private router: Router;
  constructor() {
    this.router = Router();
  }
  private createOrganization(req: Request, res: Response) {
    const organization = OrganizationModel.build(req.body as IOrganization);
    organization.save()
      .then((organizationCreated: any) => {
        res.status(200).json({ orgnaization: organizationCreated });
      })
      .catch((e: any) => {
        res.status(500).json({ error: e });
      });
  }
  private getAllOrganization(req: Request, res: Response) {
    OrganizationModel.findAll()
      .then((organizations: any[]) => {
        res.status(200).json(organizations);
      })
      .catch((e: any) => {
        console.log(e);
        res.status(500).json({ error: e });
      });
  }
  private getOrganizationById(req: Request, res: Response) {
    const organization_Id: number = req.params.id;
    OrganizationModel.findOne({
      where: { organization_Id },
      attributes: ['organization_Id', 'organization_Name', 'organization_NIT'],
      include: [{ model: RoleModel, attributes: ['role_Name', 'role_Status'] }]
    })
      .then((organization) => {
        console.log(organization);
        if (organization) {
          res.status(200).json(organization);
        } else {
          res.status(404).json({ msg: 'organization not found' });
        }
      })
      .catch((e: any) => {
        console.log(e);
        res.status(500).json({ error: e })
      });
  }
  public buildingRoutes(): Router {
    this.router.post('/', this.createOrganization);
    this.router.get('/', this.getAllOrganization);
    this.router.get('/:id', this.getOrganizationById);
    return this.router;
  }
}
export default new OrganizationRouter().buildingRoutes();