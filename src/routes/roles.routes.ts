import { Router, Request, Response } from 'express';
import { RoleModel, IRole } from '../models/roles.model';

class RoleRouter {
  private router: Router;
  constructor() {
    this.router = Router();
  }
  private getAllRoles(req: Request, res: Response) {
    RoleModel.findAll()
      .then((roles: IRole[]) => res.json(roles))
      .catch(e => res.json(e));
  }
  public buildingRoutes(): Router {
    this.router.get('/', this.getAllRoles);
    return this.router;
  }
}
export default new RoleRouter().buildingRoutes();