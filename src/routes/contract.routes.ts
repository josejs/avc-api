import { Router, Request, Response } from 'express';
import { IContract, ContractModel } from '../models/contract.model';
import { OrganizationModel } from '../models/organization.model';

class ContractRouter {
    private router: Router;
    constructor(){
        this.router = Router();
    }

    private createContract(req: Request, res: Response){
        const contract = ContractModel.build(req.body as IContract);
        contract.save()
        .then((organizationCreated: any) => {
            res.status(200).json({ organization: organizationCreated });
        })
        .catch((e: any) => {
            res.status(300).json({ error: e });
        });
    }
    private getAllContracts(req: Request, res: Response){
        ContractModel.findAll()
        .then((contracts: any[]) => {
            res.status(200).json(contracts);
        })
        .catch((e: any) => {
            res.status(300).json({error: e});
        });
    }
    private getContractById(req: Request, res: Response){
        const contract_Id: number = req.params.id;
        ContractModel.findOne({
            where: {contract_Id},
            attributes: ['contract_Id', 'contract_Document', 'contract_PaymentDate', 'contract_status'],
            include: [{ model: OrganizationModel, attributes: ['organization_Id','organization_Name', 'organization_NIT'] }]
        })
        .then((contract) => {
            console.log(contract);
            if (contract) {
                res.status(200).json(contract);
            }
            else{
                res.status(404).json('Contract not found');
            }
        })
        .catch((e: any) => {
            console.log(e);
            res.status(300).json({error: e});
        });
    }

    public buildingRoutes(): Router {
        this.router.post('/', this.createContract);
        this.router.get('/', this.getAllContracts);
        this.router.get('/:id', this.getContractById);
        return this.router;
      }
}

export default new ContractRouter().buildingRoutes();