"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const roles_model_1 = require("../models/roles.model");
class RoleRouter {
    constructor() {
        this.router = express_1.Router();
    }
    getAllRoles(req, res) {
        roles_model_1.RoleModel.findAll()
            .then((roles) => res.json(roles))
            .catch(e => res.json(e));
    }
    buildingRoutes() {
        this.router.get('/', this.getAllRoles);
        return this.router;
    }
}
exports.default = new RoleRouter().buildingRoutes();
