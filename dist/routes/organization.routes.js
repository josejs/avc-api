"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const organization_model_1 = require("../models/organization.model");
const roles_model_1 = require("../models/roles.model");
class OrganizationRouter {
    constructor() {
        this.router = express_1.Router();
    }
    createOrganization(req, res) {
        const organization = organization_model_1.OrganizationModel.build(req.body);
        organization.save()
            .then((organizationCreated) => {
            res.status(200).json({ orgnaization: organizationCreated });
        })
            .catch((e) => {
            res.status(500).json({ error: e });
        });
    }
    getAllOrganization(req, res) {
        organization_model_1.OrganizationModel.findAll()
            .then((organizations) => {
            res.status(200).json(organizations);
        })
            .catch((e) => {
            console.log(e);
            res.status(500).json({ error: e });
        });
    }
    getOrganizationById(req, res) {
        const organization_Id = req.params.id;
        organization_model_1.OrganizationModel.findOne({
            where: { organization_Id },
            attributes: ['organization_Id', 'organization_Name', 'organization_NIT'],
            include: [{ model: roles_model_1.RoleModel, attributes: ['role_Name', 'role_Status'] }]
        })
            .then((organization) => {
            console.log(organization);
            if (organization) {
                res.status(200).json(organization);
            }
            else {
                res.status(404).json({ msg: 'organization not found' });
            }
        })
            .catch((e) => {
            console.log(e);
            res.status(500).json({ error: e });
        });
    }
    buildingRoutes() {
        this.router.post('/', this.createOrganization);
        this.router.get('/', this.getAllOrganization);
        this.router.get('/:id', this.getOrganizationById);
        return this.router;
    }
}
exports.default = new OrganizationRouter().buildingRoutes();
