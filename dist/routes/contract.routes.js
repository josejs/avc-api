"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const contract_model_1 = require("../models/contract.model");
const organization_model_1 = require("../models/organization.model");
class ContractRouter {
    constructor() {
        this.router = express_1.Router();
    }
    createContract(req, res) {
        const contract = contract_model_1.ContractModel.build(req.body);
        contract.save()
            .then((organizationCreated) => {
            res.status(200).json({ organization: organizationCreated });
        })
            .catch((e) => {
            res.status(300).json({ error: e });
        });
    }
    getAllContracts(req, res) {
        contract_model_1.ContractModel.findAll()
            .then((contracts) => {
            res.status(200).json(contracts);
        })
            .catch((e) => {
            res.status(300).json({ error: e });
        });
    }
    getContractById(req, res) {
        const contract_Id = req.params.id;
        contract_model_1.ContractModel.findOne({
            where: { contract_Id },
            attributes: ['contract_Id', 'contract_Document', 'contract_PaymentDate', 'contract_status'],
            include: [{ model: organization_model_1.OrganizationModel, attributes: ['organization_Id', 'organization_Name', 'organization_NIT'] }]
        })
            .then((contract) => {
            console.log(contract);
            if (contract) {
                res.status(200).json(contract);
            }
            else {
                res.status(404).json('Contract not found');
            }
        })
            .catch((e) => {
            console.log(e);
            res.status(300).json({ error: e });
        });
    }
    buildingRoutes() {
        this.router.post('/', this.createContract);
        this.router.get('/', this.getAllContracts);
        this.router.get('/:id', this.getContractById);
        return this.router;
    }
}
exports.default = new ContractRouter().buildingRoutes();
