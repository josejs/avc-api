"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const organization_routes_1 = __importDefault(require("./organization.routes"));
const roles_routes_1 = __importDefault(require("./roles.routes"));
const contract_routes_1 = __importDefault(require("./contract.routes"));
class IndexRouter {
    constructor() {
        this.router = express_1.Router();
    }
    builderRoutes() {
        this.router.use('/organization', organization_routes_1.default);
        this.router.use('/roles', roles_routes_1.default);
        this.router.use('/contract', contract_routes_1.default);
        return this.router;
    }
}
exports.default = new IndexRouter().builderRoutes();
