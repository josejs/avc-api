"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
class Db {
    constructor() {
        this.db = new sequelize_1.Sequelize({
            port: 3306,
            dialect: 'mysql',
            username: 'admin',
            password: 'Hoobastank1997?',
            database: 'AVC_DB'
        });
    }
    authentication() {
        this.db.authenticate()
            .then(() => console.log('connected db successfull'))
            .catch((e) => console.log('error: ', e));
    }
    getInstanceDb() {
        return this.db;
    }
}
exports.default = new Db();
