"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importStar(require("express"));
const body_parser_1 = require("body-parser");
const db_1 = __importDefault(require("./db/db"));
const index_routes_1 = __importDefault(require("./routes/index.routes"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.configurationServer();
        this.routes();
        this.listenDatabase();
        this.listenServer();
    }
    configurationServer() {
        this.app.use(body_parser_1.json({ strict: true }));
        this.app.use(express_1.urlencoded({ limit: '16mb' }));
    }
    routes() {
        this.app.use('/api', index_routes_1.default);
    }
    listenDatabase() {
        db_1.default.authentication();
    }
    listenServer() {
        this.app.listen(3000, () => {
            console.log('listen server on port 3000');
        });
    }
}
exports.default = new Server();
