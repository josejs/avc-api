"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('representative', {
        represent_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        represent_Name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        represent_Identification: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            unique: true
        },
        represent_Status: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        },
        represent_OrganizationID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'organization',
                key: 'organization_Id'
            },
            unique: true
        }
    }, {
        tableName: 'representative'
    });
};
