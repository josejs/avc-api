"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('payment_details', {
        paymentDetail_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        paymentDetail_ContractID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'contract',
                key: 'contract_Id'
            },
            unique: true
        },
        paymentDetail_PaymentMethodID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'payment_method',
                key: 'paymentMethod_Id'
            }
        },
        paymentDetail_Value: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        paymentDetail_Description: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        paymentDetail_IsPaid: {
            type: DataTypes.INTEGER(1),
            allowNull: false
        }
    }, {
        tableName: 'payment_details'
    });
};
