"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_1 = __importDefault(require("../db/db"));
const sequelize = db_1.default.getInstanceDb();
class RoleModel extends sequelize_1.Model {
    static init() {
        super.init({
            role_Id: {
                type: sequelize_1.DataTypes.INTEGER({ length: 11 }),
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            role_Name: {
                type: sequelize_1.DataTypes.STRING({ length: 45 }),
                allowNull: false,
            },
            role_Status: {
                type: sequelize_1.DataTypes.TINYINT({ length: 1 })
            }
        }, { tableName: 'Roles', timestamps: false, freezeTableName: true, sequelize });
    }
}
exports.RoleModel = RoleModel;
RoleModel.init();
