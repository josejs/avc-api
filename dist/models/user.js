"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user', {
        organization_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        organization_Name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        organization_NIT: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            unique: true
        },
        organization_Direction: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        organization_Email: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        organization_Password: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        organization_Status: {
            type: DataTypes.INTEGER(4),
            allowNull: true
        },
        organization_RoleID: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        }
    }, {
        tableName: 'user'
    });
};
