"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('contract', {
        contract_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        contract_Document: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        contract_Date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        contract_PaymentDate: {
            type: DataTypes.DATE,
            allowNull: false
        },
        contract_Status: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        },
        contract_OrganizationID: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'organization',
                key: 'organization_Id'
            }
        }
    }, {
        tableName: 'contract'
    });
};
