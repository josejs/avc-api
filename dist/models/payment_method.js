"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('payment_method', {
        paymentMethod_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        paymentMethod_Name: {
            type: DataTypes.STRING(100),
            allowNull: false
        }
    }, {
        tableName: 'payment_method'
    });
};
