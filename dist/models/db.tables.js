"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = __importStar(require("path"));
exports.getModels = function (seq) {
    const tables = {
        contract: seq.import(path.join(__dirname, './contract')),
        organization: seq.import(path.join(__dirname, './organization')),
        payment_details: seq.import(path.join(__dirname, './payment_details')),
        payment_method: seq.import(path.join(__dirname, './payment_method')),
        representative: seq.import(path.join(__dirname, './representative')),
        roles: seq.import(path.join(__dirname, './roles')),
        user: seq.import(path.join(__dirname, './user')),
    };
    return tables;
};
