"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('roles', {
        role_Id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        role_Name: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        role_Status: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        }
    }, {
        tableName: 'roles'
    });
};
