"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const bcrypt_1 = require("bcrypt");
const db_1 = __importDefault(require("../db/db"));
const roles_model_1 = require("./roles.model");
const db = db_1.default.getInstanceDb();
class OrganizationModel extends sequelize_1.Model {
    static init() {
        super.init({
            organization_Id: {
                type: sequelize_1.DataTypes.INTEGER({ length: 11 }),
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            organization_Name: {
                type: sequelize_1.DataTypes.STRING(100),
                allowNull: false,
            },
            organization_NIT: {
                type: sequelize_1.DataTypes.INTEGER({ length: 11 }),
                unique: true,
                allowNull: false,
            },
            organization_Direction: {
                type: sequelize_1.DataTypes.STRING(100),
                allowNull: false,
            },
            organization_Email: {
                type: sequelize_1.DataTypes.STRING(100),
                allowNull: false
            },
            organization_Password: {
                type: sequelize_1.DataTypes.TEXT({ length: 'long' }),
                allowNull: false
            },
            organization_Status: {
                type: sequelize_1.DataTypes.TINYINT({ length: 1 }),
                allowNull: false
            },
            organization_RoleID: {
                type: sequelize_1.DataTypes.INTEGER({ length: 11 }),
                references: {
                    model: roles_model_1.RoleModel,
                    key: 'role_Id'
                }
            }
        }, {
            tableName: 'Organization',
            freezeTableName: true,
            timestamps: false,
            sequelize: db,
            hooks: {
                beforeCreate: (organization, options) => {
                    const passwordHashed = bcrypt_1.hashSync(organization.organization_Password, bcrypt_1.genSaltSync(5));
                    organization.organization_Password = passwordHashed;
                },
            },
        });
    }
}
exports.OrganizationModel = OrganizationModel;
OrganizationModel.init();
OrganizationModel.belongsTo(roles_model_1.RoleModel, {
    foreignKey: 'organization_RoleId',
    targetKey: 'role_Id'
});
