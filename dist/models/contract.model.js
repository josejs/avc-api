"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db_1 = __importDefault(require("../db/db"));
const organization_model_1 = require("./organization.model");
const db = db_1.default.getInstanceDb();
class ContractModel extends sequelize_1.Model {
    static init() {
        super.init({
            contract_Id: {
                type: sequelize_1.DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            contract_Document: {
                type: sequelize_1.DataTypes.TEXT({ length: 'long' }),
                allowNull: false
            },
            contract_Date: {
                type: 'TIMESTAMP',
                allowNull: true
            },
            contract_PaymentDate: {
                type: sequelize_1.DataTypes.DATE,
                allowNull: false
            },
            contract_Status: {
                type: sequelize_1.DataTypes.BOOLEAN,
                allowNull: true
            },
            contract_OrganizationID: {
                type: sequelize_1.DataTypes.INTEGER,
                references: {
                    model: organization_model_1.OrganizationModel,
                    key: 'organization_Id'
                },
                allowNull: false
            }
        }, {
            tableName: 'Contract',
            freezeTableName: true,
            sequelize: db,
            timestamps: false
        });
    }
}
exports.ContractModel = ContractModel;
ContractModel.init();
ContractModel.belongsTo(organization_model_1.OrganizationModel, {
    foreignKey: 'contract_OrganizationID',
    targetKey: 'organization_Id'
});
